""" In charge of :
    1. Running the malfunction inspection logic periodically.
    2. Sending mail when the new alert detected.
    3. Writing the html files to support old mail reports as well as current status (the viewing is done using a simple python file server that serves those html files).
"""

import os, socket, glob, time, ctypes
import socket, hashlib, select, traceback, shutil
import sys, logging
import struct, errno
import time
from pymongo import MongoClient
import pymongo
import datetime
from datetime import timedelta
import logging.handlers
import json
from collections import OrderedDict
import subprocess
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders

# Test the module system_inspector compiles
import system_inspector

html_start  = r"""<html><head><meta http-equiv="refresh" content="15"><meta charset="utf-8"><style></style></head><body lang=EN-US><div class=WordSection1>"""
html_end    = r"""</div></body></html>"""
DEFAULT_WAIT_BEFORE_KILL = 30
colors = {'green': '#00B050', 'orange': '#FF9900', 'grey': '#A6A6A6', 'red': 'red', 'black': 'black', 'blue': 'blue'}

mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
default_admin = ["oded", 'kyteranotify']

SERVER_URL = mongo_client['companion']['cloud_config'].find_one({"component" : "malfunction", "server_name" : socket.gethostname()})['params']['url']
envs  = list(mongo_client['companion']['cloud_config'].distinct("environment", {"component" : "malfunction", "server_name" : socket.gethostname()}))
confs = list(mongo_client['companion']['systems'].find({"env": {"$in": envs} }, {"users.admin":1, "sysid":1 }))

admins_mails = dict([(c["username"], c["email"]) for c in 
    list(mongo_client['companion']['users'].find(
    {"username": {"$in": 
        list(reduce(set.union,[set(c.get('users',{}).get('admin',[])) for c in confs], set(default_admin)))}
    }, {"username":1, "email":1}))])
    
sysids  = dict([(c['sysid'], list(set(map(lambda admin: admins_mails.get(admin, "unknown_user_%s"%admin) ,c.get('users',{}).get('admin',[]) + default_admin))) ) for c in confs])

if 'debug' in map(str.lower, sys.argv):
    for k, v in sysids.iteritems():
        sysids[k] = [oded_mail]

def run_malfunction(sysid, output_file):
    exit_code = 0
    terminate = False
    if sys.platform == 'win32':
        p = subprocess.Popen("python system_inspector.py %s %s" % (sysid, output_file))
    else:
        p = subprocess.Popen(["/usr/bin/python", 'system_inspector.py', sysid, output_file])    
    start_time = time.time()
    
    # Wait the process finish or timeout
    wait_before_kill = DEFAULT_WAIT_BEFORE_KILL if 'debug' not in sys.argv[1:] else 0xfffffffff
    while (p.poll() is None and time.time() - start_time < wait_before_kill):
        time.sleep(5)
    
    if p.poll() is not None:
        exit_code = p.poll()
    else:
        # The process is stuck
        terminate = True
        os.kill(p.pid, 9)
        time.sleep(0.5)
        exit_code = p.poll()

    if not terminate and exit_code != 0:
        exit_code = errno.errorcode[exit_code]
        
    return terminate, exit_code
    
def load_result(result_file_path):
    with file(result_file_path, 'rb') as f:
        return json.load(f,  object_pairs_hook=OrderedDict)

def create_summary(tests, result_statistics):
    summary = []
    #record = (name, color, prefixLetter, tabs, childs)
    summaryDict = {}
    result_statistics.count_systems = result_statistics.count_systems + 1

    for name, test in tests.iteritems():
        #name = n
        #if t.get('alert_reason', '') != '':
        #    name = '%s (%s)' % (n, t['alert_reason']) # Extended information.
        tabs = 0
        prefixLetter = '  '
        inserted = False
        
        result_statistics.count_tests = result_statistics.count_tests + 1
        
        if 'exc' in test and test['exc']:
            prefixLetter = 'EX'
            color = 'black'
            result_statistics.count_exception = result_statistics.count_exception + 1
        elif 'run' in test and test['run'] is False:
            prefixLetter = 'Not run'
            color = 'grey'
            result_statistics.count_not_run = result_statistics.count_not_run + 1
            # Add after the failed test
            failed_test = ''
            if 'failed_true_prerequisite' in test:
               failed_test = test['failed_true_prerequisite'][0]
            elif 'failed_false_prerequisite' in test:
               failed_test = test['failed_false_prerequisite'][0] 
            
            if failed_test != '':
                f_name, f_test, f_color, f_prefixLetter, f_tabs, f_childs = summaryDict[failed_test]
                
                tabs = f_tabs + 1
                record = (name, test, color, prefixLetter, tabs, [])
                f_childs.append(record)
                summaryDict[failed_test] = f_name, f_test, f_color, f_prefixLetter, f_tabs, f_childs
                summaryDict[name] = record
                inserted = True 
        elif 'should_alert' in test and test['should_alert'] is True:
            prefixLetter = 'Fail'
            color = 'red'
            result_statistics.count_fail = result_statistics.count_fail + 1
        elif 'result' in test and test['result'] is False:
            prefixLetter = 'False'
            color = 'blue'
        else:
            from system_inspector import CheckSucess
            if test['check_type'] == CheckSucess.__name__:
                prefixLetter = 'True'
                color = 'orange'
            else:
                prefixLetter =  'Pass'
                color = 'green'
            result_statistics.count_success = result_statistics.count_success + 1

        if inserted is False:
            record = name, test, color, prefixLetter, tabs, []
            summary.append(record)
            summaryDict[name] = record

    return summary

def generate_html_footers(summary, res, full_report):
    checks_explanation = ["%s: %s" % (check.name, check.explanation) for check in system_inspector.get_check_list()]
    checks_explanation = """<p class=MsoListParagraphCxSpMiddle style='margin-left:0in'>&nbsp;</p> <table border="1" style="width:100%%">"""
    checks = system_inspector.get_check_list()
    for i in range(len(checks)):
        checks_explanation = checks_explanation + "<tr><td>%d</td><td>%s</td><td>%s</td></tr>" % (i, checks[i].name, checks[i].explanation)
    checks_explanation = checks_explanation + '</table>'

    marks_explanation = \
"""<p class=MsoListParagraphCxSpMiddle style='margin-left:0in'>&nbsp;</p>Explanation:
<p>V: Test result was true (green)</p>
<p>X: Test result was false (orange)</p>
<p>->: Test didn't run(grey)</p>
<p>AL: This test result created alert</p>
<p>EX: The test failed with exception and didn't finish</p>
"""
    full_report_table = ''
    if full_report is True:
        full_report_table = """<p class=MsoListParagraphCxSpMiddle style='margin-left:0in'>&nbsp;</p> """
        checks_explanation = ''
        for (name, test, color, prefixLetter, tabs, childs) in summary:
            name = filter(lambda t: name.startswith(t), res['tests'].keys())[0] # Ugly: Get real name from summery (use summary order).
            t = res['tests'][name] # the test object
            if t.get('run', True) is False:
                continue

            explanation = filter(lambda c: c.name == name, checks)[0].explanation
            full_report_table = full_report_table + """<h2 id="%s">%s: <span style='color:%s'>%s</span></h2><p style='margin-left:0in'>%s</p><table border="1" style="width:100%%">""" % (name.replace(' ', '_'), name, colors[color], prefixLetter, explanation)
            
            for k,v in t.iteritems():
                if v != '' and v!= [] and v!= {} and v is not None and k not in ['check_type', 'should_alert', 'end_time_utctime', 'start_time_utctime']: # Include False, 0, 0.0, etc...
                    # Some fix for float
                    if type(v) is float:
                        v = '{:f}'.format(v)
                    full_report_table = full_report_table + "<tr><td>%s</td><td>%s</td></tr>" % (k, v)
            full_report_table = full_report_table + """</table>"""

    #return marks_explanation.replace('\n', '\r\n') + os.linesep + checks_explanation + full_report_table
    return checks_explanation + full_report_table

def convert_summary_to_html(sysid, add_checks_explanation, res, recenty_restarted, summary, old_summary, html_output, link_path, terminate, exit_code):
    old_summary_dict = {}
    link = r'%s:60010/%s' % ( SERVER_URL, link_path.replace(' ', '%20').replace(os.linesep, '/'))
    
    if old_summary is not None:
        for (name, test, color, prefixLetter ,_ , childs) in old_summary:
            old_summary_dict[name] = (prefixLetter, color)

    # Create HTML page with the results
    
    helper = lambda test_name, field_name: res.get('tests', {}).get(test_name, {}).get(field_name, '')
    
    last_ip     = helper('LmuApp checkin', 'lastIp')
    position    = helper('New position is available', 'last_position_room')
    posture     = helper('New posture is available', 'last_stance')
    at_home     = helper('Out of home indication', 'at_home_indication')
    wear_detect = helper('battery is ok', 'wear_detect')
    alert_indication = helper('battery is ok', 'alert_indication')
    
    summaery_status = "".join([
"<h2>Summary:</h2>"
"<p class=MsoNormal>Sysid: %s</p>" % sysid,
"<p class=MsoNormal>Test Start Time = %s</p>" % res['start_time'],
"<p class=MsoNormal>Test Duration(sec) = %s</p>" % res['duration'],
"<p class=MsoNormal>last_ip = %s</p>"  % (last_ip if last_ip != '' else 'Unavailable'),
"<p class=MsoNormal>position = %s</p>" % position if position != '' else '',
"<p class=MsoNormal>posture = %s</p>"  % posture if posture != '' else '',
"<p class=MsoNormal>at_home = %s</p>"  % at_home if at_home != '' else '',
"<p class=MsoNormal>wear_detect = %s</p>" % wear_detect if wear_detect != '' else '',
"<p class=MsoNormal>alert_indication = %s</p>""" % alert_indication if alert_indication != '' else '',
 ])

    html_template = (html_start+summaery_status +
"""%(headers)s
<p class=MsoNormal>&nbsp;</p>
%(summery_lines)s
%(footers)s
<p class=MsoListParagraphCxSpMiddle style='margin-left:0in'>&nbsp;</p>
<p class=MsoListParagraphCxSpLast style='margin-left:0in'>&nbsp;</p>"""+html_end).decode('utf-8')

    headers = []
    if terminate is True or exit_code!= 0:
        if terminate is True:
            headers.append('Test was brutally terminated.')
        if exit_code != 0:
            headers.append('Test finished unsuccefully: errno:%s.' % exit_code)
            
    if recenty_restarted is True: 
        headers.append('<h2>The LmuApp started recently(<30min). Might be maintainace.')
    #html_summery_lines = []
    html_summery_lines = ["""<table border="3" style="width:100%%">"""]
    html_summery_lines.append("""<td>Former run</td><td>Run verdict</td><td>Test</td>""")

    def create_html_summery_lines(summary_arg, tabs):
        for (name, test, color, prefixLetter,_ , childs) in summary_arg:
            if prefixLetter == 'Not run' and test['check_type'] == system_inspector.CheckSucess.__name__:
                create_html_summery_lines(childs, tabs)
            else:
                html_summery_lines.append('<tr>')
                old_prefixLetter, old_color = old_summary_dict.get(name, ('', 'black'))
                html_summery_lines.append("""<td><span style='color:%s'>%s</td>""" % (colors[old_color], old_prefixLetter))
                html_summery_lines.append("""<td><span style='color:%s'>%s</td>""" % (colors[color], prefixLetter))
            
                if test.get('alert_reason', '') != '':
                    description = '%s (%s)' % (name, test['alert_reason']) # Extended information.
                else:
                    description = name

                if prefixLetter != 'Not run':
                    html_summery_lines.append(("""<td></td>""" * tabs) +  """<td><link><a href="%s#%s">%s</a></link></td>""" % (link, name.replace(' ', '_'), description))
                else:
                    html_summery_lines.append(("""<td></td>""" * tabs) +  """<td>%s</td>""" % (description))
                    
                html_summery_lines.append('</tr>')
                
                create_html_summery_lines(childs, tabs+1)

    create_html_summery_lines(summary, 0)
    html_summery_lines.append('</table>')

    if html_output is not None:
        with file(os.path.join(os.path.dirname(__file__), html_output), 'wb') as f:    
            footers = generate_html_footers(summary, res, full_report=True)
            html_page = (html_template % ({
                              'summery_lines': ''.join(html_summery_lines),
                              'headers': os.linesep.join(headers),                              
                              'footers': footers 
                              })).encode('utf-8')
            f.write(html_page)
    
    return (html_template % ({'summery_lines': ''.join(html_summery_lines),
                              'headers': os.linesep.join(headers),                              
                              'footers': generate_html_footers(summary, res, full_report=False) if add_checks_explanation else ''
                              })).encode('utf-8')

def get_file_link(report_file):
    return '<p><link><a href="http://beta2.kyteratech.com:60010/%s">view full report</a></link></p>' \
        % (report_file.replace(' ', '%20').replace(os.linesep, '/'))

class MalFunctionMail(object):
    def __init__(self, sysid):
        self.sysid = sysid
        self.to = sysids[self.sysid]
        self.gmail_user = 'KyteraMailer@gmail.com'
        self.gmail_pwd = 'nbGu8Db3k1'

        self.content = []
        
    def attach_info(self, html_summery_content, full_report_file, recently_restarted, alert_id):
        self.content.append(MIMEText(html_summery_content + get_file_link(full_report_file), 'html'))
        self.recently_restarted = recently_restarted
        self.alert_id = alert_id
        #self.content.append(MIMEText(get_file_link(full_report_file), 'html'))
        # Attach the original file as well
        #part = MIMEBase('application', "octet-stream")
        #part.set_payload(open("%s" % full_report_file, "rb").read())
        #Encoders.encode_base64(part)
        #part.add_header('Content-Disposition', 'attachment; filename="%s"' % full_report_file)
        #self.content.attach(part)

    def send_mail(self, rs, mail_sending_delay):
        msg = MIMEMultipart('multipart') #multipart|alternative?
        msg['From'] = self.gmail_user
        msg['To'] = ','.join(self.to)

        msg['Subject'] = "[Malfunction-Alert] System \"%s\" state changed" % (self.sysid)
        delay_header = 'AlertId: %d' % self.alert_id
        
        if self.recently_restarted is True:
            msg['Subject'] = "[Malfunction-warning] System \"%s\" state changed" % (self.sysid)
            delay_header = delay_header + "The system had a restart recenly, so the malfunction might be irrelevent."
        
        if mail_sending_delay >= 5:
            delay_header = delay_header + """ <h2>Unexpected mail sending problem caused a delay of %d seconds to malfunction functionaliy.</h2>""" % mail_sending_delay

        run_success = delay_header + """<h2>%d/%d systems ran successfully:</h2>""" % (len(rs.systems_passed), rs.count_systems) +\
            ''.join(["<p class=MsoNormal><span style='color:#00B050'>%s</span></p>" % sysid for sysid in rs.systems_passed]) if len(rs.systems_passed) > 0 else ''
        
        map(msg.attach, [MIMEText(run_success, 'html')] + self.content)
        
        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server_ssl.ehlo() # optional, called by login()
        server_ssl.login(self.gmail_user, self.gmail_pwd)
        # ssl server doesn't support or need tls, so don't call server_ssl.starttls() 
        print 'Sending Mail to: %s' % msg['To']

        server_ssl.sendmail(self.gmail_user, self.to, msg.as_string())
        server_ssl.quit()
        server_ssl.close()

class ResultsStatistics(object):
    def __init__(self):
        self.count_success = 0
        self.count_fail = 0
        self.count_not_run = 0
        self.count_exception = 0
        self.count_tests = 0
        self.count_systems = 0
        self.systems_passed = []
        self.systems_not_passed = []

class AllSystemsSummary(object):
    """ PATCH!!! TODO: make it integrated.
    """
    def __init__(self):
        self.buff=''

    def add_system_summary(self, summery_html, html_file):
        self.buff = self.buff + (summery_html.
            replace(html_start,   '').
            replace(html_end,     get_file_link(os.path.basename(html_file))))

    def close(self):
        with file(os.path.join('malfunction_html', 'all_systems_summary.html'), 'wb') as f:
            f.write(html_start+self.buff+html_end)
            
def update_malfunction_tickets(sysid, res, terminate):
    open_tickets = map(lambda t: t['name'], list(mongo_client['companion']['malfunction_tickets'].find({'sysid' : sysid, 'status': 'open'})))
    
    to_close_list = []
    for open_ticket in open_tickets:
        if res['tests'].get(open_ticket, {}).get('should_alert', False) is False:
            to_close_list.append(open_ticket)
    
    for ticket in to_close_list:
        mongo_client['companion']['malfunction_tickets'].find_one_and_update(
                {'sysid': sysid, 'name': str(ticket), 'status': 'open'},
                {"$set": 
                    {'status': 'closed', 
                     'close_timestamp': datetime.datetime.fromtimestamp(res.get('end_time_utctime', 0)),
                 }#'malfunction_duration': (res['end_time'] - x[0]['timestamp']).total_seconds()}
                })
    
    # Try to open new tickets
    to_add_list = [(name,t) for (name,t) in res['tests'].iteritems() if t.get('should_alert', False) is True and name not in open_tickets]
    #print sysid, "to_add_list: ", to_add_list, "open_tickets:", open_tickets
    
    for (name,t) in to_add_list:
        last_id = list(mongo_client['companion']['malfunction_tickets'].find({}).sort('timestamp',-1).limit(1))
        if len(last_id) == 0:
            new_id = 0
        else:
            new_id = last_id[0]['ticket_id'] + 1
            
        mongo_client['companion']['malfunction_tickets'].insert(
                {'sysid': sysid,
                'status': 'open', 
                'name': name,
                'close_timestamp': None,
                'ticket_id': new_id,
                'alert_reason':t['alert_reason'],
                'alert_msg': t['alert_msg'],
                'user_message': t.get('user_message', ''),
                'timestamp': datetime.datetime.fromtimestamp(res.get('start_time_utctime', 0)),
                })


def send_marks_window_message_from_malfunction(sysid, text):
    import json, httplib
    headers = {'Content-type': 'application/json'}
    
    message = {'cmd': 'ui show msg',
     'token': 'malfunctionToken',
     'data': {'msg': text,
              'sysid': sysid}}
    conn = httplib.HTTPConnection('10.8.0.1', 60001, timeout = 10)
    conn.request('POST', '/algocmd', body=json.dumps(message), headers=headers)
    res = json.loads(conn.getresponse().read().decode('utf-8'))
    if res['result'] == 'System not logged in for fingerprinting':
        return

    assert res['result'] == 'ok'
    return
    
def update_malfunction_alert_state(sysid, res, terminate):
    default_msg = 'We detected problem with your system and we are working to fix it'
    alert_name = ''
    ticket_id = 0
    user_message = ''
 
    open_tickets = list(mongo_client['companion']['malfunction_tickets'].find({'sysid' : sysid, 'status':'open'}))
    open_tickets_ordered = []
    open_tickets_names = dict([(open_ticket['name'], open_ticket) for open_ticket in open_tickets])
    
    for name in res['tests'].keys():
        if name in open_tickets_names:
            open_tickets_ordered.append(open_tickets_names[name])
    
    should_alert = res['should_alert']
    
    if should_alert is True:
        alert_msg = default_msg
        alert_reason = default_msg
        user_message = "System is temporarily not working"
        # Find the first with explanation
        ticket = open_tickets_ordered[0]
        alert_name = ticket['name']
        alert_msg  = ticket['alert_msg']
        alert_reason = ticket['alert_reason']
            
        if alert_reason == '':
            alert_reason = alert_msg
        ticket_id  = ticket['ticket_id']
        
        try:
            send_marks_window_message_from_malfunction(sysid, "Malfunction %d: %s" % (ticket_id, alert_reason))
        except:
            traceback.print_exc()
            
        if ticket.get('user_message', '') != '':
            user_message = ticket['user_message']
    else:
        alert_msg = ''
        alert_reason = ''
        
        helper = lambda test_name, field_name: res.get('tests', {}).get(test_name, {}).get(field_name, '')
        
        output_class = helper('Wristband is at home', 'output_class')
        
        if output_class != 'return':
            send_marks_window_message_from_malfunction(sysid, "Wristband is not at home: %s" % output_class)
        else:
            wri_mode = helper('Wristband current mode include accl data', 'wri_mode')
            
            if wri_mode != 'distress':
                send_marks_window_message_from_malfunction(sysid, "Wristband mode is %s!!" % wri_mode)
            else:
                send_marks_window_message_from_malfunction(sysid, "No malfunction. wri in distress mode")

    if terminate is True:
        res['should_alert'] = True
        should_alert = True
        alert_msg = 'Temporary system issue'
        alert_reason = 'Malfunction test terminated in the middle'
        alert_name = 'Running malfunction'
        ticket_id  = 0
        res['end_time_utctime'] = time.time()
        
    last_record = list(mongo_client['companion']['fmtalert'].find({'sysid' : sysid, 'type':'malfunction'}).sort('timestamp', -1).limit(1))
    
    generate_new_record = False
    state_changed = False
    alertid = 0
    
    if len(last_record) == 0:
        alertid = 0
        generate_new_record  = True
    else:
        changing_alert_reason = (alert_name != last_record[0]['details'].get('alert_name', "alert_name"))

        if (changing_alert_reason is True) and (last_record[0]['status'] == 'open'):
        # Close ticket
            mongo_client['companion']['fmtalert'].find_one_and_update(
                {'sysid': sysid, 'type': 'malfunction', 'alertid': last_record[0]['alertid']},
                {"$set":
                    {'status': 'closed',
                    'close_timestamp': datetime.datetime.fromtimestamp(res.get('end_time_utctime', 0)),
                 }#'malfunction_duration': (res['end_time'] - x[0]['timestamp']).total_seconds()}
                }, upsert= True)
            state_changed = True
            
            try:
                send_marks_window_message_from_malfunction(sysid, "Malfunction closed. All OK!!")
            except:
                traceback.print_exc()
                
        if should_alert is True and ((changing_alert_reason is True) or (last_record[0]['status'] == 'closed')): # There is a new alert
            generate_new_record = True
            alertid = last_record[0]['alertid'] + 1

    if generate_new_record is True:
        state_changed = True
        mongo_client['companion']['fmtalert'].insert(
                {'schema_version': 5, 
                'sysid' : sysid,
                'type' : 'malfunction',
                'alertid': alertid,
                'details': {'subtitle': "%d: %s" % (alertid, user_message),
                            'extended_subtitle': "%d: %s" % (alertid, alert_reason),
                            'ticket_id': ticket_id,
                            'alert_name': alert_name,},
                'status': 'open' if should_alert else 'closed',
                'timestamp': datetime.datetime.fromtimestamp(res.get('start_time_utctime', 0))})
                
    #mongo_client.close()
    return state_changed, alertid

def main():
    output = OrderedDict()
    
    rs = ResultsStatistics()
    old_summary = None
    should_alert = False
    all_systems_summary = AllSystemsSummary()
    
    mail_to_send = []
    
    for sysid in sysids:
        output_file ='%s_malfunction.txt' % sysid
        
        m = MalFunctionMail(sysid)
        
        try:
            old_res = load_result(output_file)
            old_rs = ResultsStatistics()
            old_summary = create_summary(old_res['tests'], old_rs)
        except:
            old_res = {'tests': {}}
            print 'Failed to load old results'

        terminate, exit_code = run_malfunction(sysid, output_file)
        #terminate = False
        #exit_code = 0
        
        should_report = False
        
        if terminate is True:
            should_report = True
            res = old_res # Use old results.
            recenty_restarted = False
            summary = old_summary
        else:
            res = load_result(output_file)
            
            recenty_restarted = res.get('tests',{}).get('LmuApp checkin',{}).get('recently_restarted', False)
            
            summary = create_summary(res['tests'], rs)
            
            if res['should_alert'] is False:
                rs.systems_passed.append(sysid)
            else:
                rs.systems_not_passed.append(sysid)
            update_malfunction_tickets(sysid, res, terminate)
            
        stat_changed, alertid = update_malfunction_alert_state(sysid, res, terminate)
        
        if stat_changed is True:
            year, month,_ = datetime.date.today().isocalendar()
            path = os.path.join('reports', '%d_%d' % (year, month), '%s_%s.html' % (sysid, time.ctime().replace(':', '_')))
            html_file = os.path.join(os.path.dirname(__file__), 'malfunction_html', path)
            directory = os.path.dirname(html_file)
            
            if os.path.exists(directory) is False:
                os.mkdir(directory)
                #shutil.copy(html_file, full_path) TODO: remove it?
        else:
            html_file = os.path.join(os.path.dirname(__file__), 'malfunction_html', output_file.replace('_malfunction.txt', '.html'))
            path = os.path.basename(html_file)
            
        summery_html = convert_summary_to_html(sysid, False, res, recenty_restarted, summary, old_summary, html_file, path, terminate, exit_code)
        all_systems_summary.add_system_summary(summery_html, html_file)
        
        if stat_changed is True:
            m.attach_info(summery_html, path, recenty_restarted, alertid)
            mail_to_send.append(m)
            should_alert = True
    
    for m in mail_to_send:
        start_mail_time = time.time()
        mail_sent = False
        while not mail_sent:
            try:
                m.send_mail(rs, time.time() - start_mail_time) # Report problems...
                mail_sent = True
            except:
                mail_sent = False
                print 'Mail sending failed...'
                traceback.print_exc()
                time.sleep(5)
    if len(mail_to_send) == 0:
        print 'Nothing changed', time.ctime()

    all_systems_summary.close()

if __name__ == "__main__":
    main()
