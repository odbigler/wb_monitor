import time, os, smtplib, socket
import traceback
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders

import pymongo
from pymongo import MongoClient
from utils import config_service
import sys, logging
import logging.handlers
from threading import Timer
import bisect
LOG_PATH                        = "battery_trigger.log"
logger                          = logging.getLogger('Battery_trigger')

# Figure out what sysids to work with
mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")

default_admin = ["oded"]

envs  = list(mongo_client['companion']['cloud_config'].distinct("environment", {"component" : "battery_trigger", "server_name" : socket.gethostname()}))
confs = list(mongo_client['companion']['systems'].find({"env": {"$in": envs} }, {"users.admin":1, "sysid":1 }))

admins_mails = dict([(c["username"], c["email"]) for c in 
    list(mongo_client['companion']['users'].find(
    {"username": {"$in": 
        list(reduce(set.union,[set(c.get('users',{}).get('admin',[])) for c in confs], set(default_admin)))}
    }, {"username":1, "email":1}))])
    
sysids  = dict([(c['sysid'], list(set(map(lambda admin: admins_mails.get(admin, "unknown_user_%s"%admin) ,c.get('users',{}).get('admin',[]) + default_admin))) ) for c in confs])
#print sysids

## Setting up logger
formmater = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger.setLevel(logging.DEBUG)
fh = logging.handlers.RotatingFileHandler(LOG_PATH, maxBytes = 10 * 1024 * 1024, backupCount = 5)

fh.setLevel(logging.DEBUG)
fh.setFormatter(formmater)
logger.addHandler(fh)

logger.info("Battery_trigger Started!")

INTERACTIVE_TIME = 30 * 60

# Wristband charging states
STATE_NOT_CHARGING      = 0
STATE_CHARGING          = 1
STATE_ZIGZAG            = 2
STATE_SUSPECT_ZIGZAG    = 3
STATE_FULL              = 4
PULL_OUT_OF_CHARGER     = 5
CHARGER_POWER_OUT       = 6 # Charger power is out during charging
CHARGER_NO_POWER        = 7 # Placed on new charger with no power
UNKNOWN_CHARGING_STATE  = 8

config = config_service.load_config('battery.conf')
timers = {}

def now():
    return datetime.utcnow()
    
# Helper function
def compare_records(old_record, new_record, ignore_list):
    if old_record is None:
        return True
    old_record = old_record.copy()
    new_record = new_record.copy()
    
    [(old_record.pop(field, None), new_record.pop(field, None)) for field in ignore_list]
    
    if new_record != old_record:
        for k in old_record.keys():
            if old_record[k] != new_record.get(k, None):
                print ("new record field %s updated:"%k) , old_record[k], new_record.get(k, None)
                break
    return new_record == old_record

class SystemBattery(object):
    systems = {}

    def __init__(self, sysid, mongo_client):
        self.sysid = sysid
        self.last_update = 0
        self.level = 'charged'
        self.is_low = False
        self.is_charging = False
        self.is_dead = True
        self.charging_est = {}
        
        self.gmail_user = 'KyteraMailer@gmail.com'
        self.gmail_pwd = 'nbGu8Db3k1'
        
        self.last_batery_report = list(mongo_client['companion']['battery'].find({'sysid': self.sysid, 'timestamp':{'$lt': now()}}).sort('time', -1).limit(1))
        self.wear_detect = 3
        
        default_last_batery_report = {'time': time.time(), 
            'battery_state': 'normal_battery',
            'timestamp': now(),
            'wear_detect': 3, 
            'in_charger': False, 
            'charging_done': True, 
            'charging_full': False, 
            'is_charging': False}
        
        if len(self.last_batery_report) == 1:
            self.last_batery_report = self.last_batery_report[0]
            del self.last_batery_report['_id']
            for k,v in default_last_batery_report.iteritems():
                self.last_batery_report.setdefault(k, v)
        else:
            self.last_batery_report = default_last_batery_report
        
        global last_report_time
        last_report_time[sysid] = self.last_batery_report

    @staticmethod
    def get(sysid, mongo_client):
        if sysid not in SystemBattery.systems:
            SystemBattery.systems[sysid] = SystemBattery(sysid, mongo_client)
        return SystemBattery.systems[sysid]
        
    def get_level_estimation(self, volts):
        from battery_level_estimation import battery_estimation 
             
        # Skip fw version
        estimation = battery_estimation[0]
        
        index = bisect.bisect(estimation['volts'], volts)
        
        if index == len(estimation['volts']):
            index = index - 1
        
        return estimation['hours_left'][index], estimation['battery_percentage'][index]
        
    def get_battery_state(self, report, is_dead, placed_on_charger_lately):
        interactive = placed_on_charger_lately
        
        if is_dead:
            is_charging = False
            in_charger  = False
            level = config['EMPTY']
            original_level = 0
            hours_left = 0
            battery_state = 'empty_battery'
            wear_detect = self.wear_detect
            original_level_volts = 0
        else:
            wear_detect = report['params'].get('wear_detect', 0)
            #if wear_detect == 1 and self.wear_detect == 0:
            #    interactive = True
            self.wear_detect = wear_detect
            original_level_volts = report['params']['batteryLevelVolts']
            hours_left, original_level = self.get_level_estimation(report['params']['batteryLevelVolts'])
            hours_left = max(1, hours_left - config['EMPTY_THRESHOLD'])
            self.last_update = report['timestamp']
            
            print (self.sysid, 'hours_left', hours_left, 'original_level', original_level)
            
            if hours_left <= config['VERY_LOW_THRESHOLD']:
                level, battery_state =  config['VERY_LOW'], 'very_low_battery'
            elif hours_left <= config['LOW_THRESHOLD'] :
                level, battery_state =  config['LOW'], 'low_battery'
            elif original_level <= config['MEDIUM_THRESHOLD']:
                level, battery_state =  config['MEDIUM'], 'medium_battery'
            elif original_level <= config['GOOD_THRESHOLD']:
                level, battery_state =  config['GOOD'], 'normal_battery'
            else:
                level, battery_state = config['FULL'], 'normal_battery'
                
            charging_possible_status = [STATE_CHARGING, STATE_FULL]
            
            is_charging = report['params']['charging'] in charging_possible_status
            in_charger  = is_charging or report['params']['charging'] in [CHARGER_POWER_OUT, CHARGER_NO_POWER]

            if is_charging is True and original_level <= config['MEDIUM_THRESHOLD']:
                battery_state = 'battery_charging'

        values = {'battery_level': original_level,
                  'battery_level_volts': original_level_volts,
                  'battery_level_rough': level,
                  'hours_left': hours_left,
                  'hours_left_rough': round(hours_left),
                  'sysid': self.sysid,
                  'is_charging': is_charging,
                  'in_charger': in_charger,
                  'wear_detect': wear_detect,
                  'time': time.time(),
                  'timestamp': now(),
                  'interactive': interactive,
                  'source': 'low_battery_detection',
                  'level': level,
                  'charging_time_estimation':None,
                  'charging_end_time_estimation':None,
                  'type': 'detected',
                  'charging_full' : False,
                  'charging_done' : None,
                  'battery_state': battery_state,}

        if is_charging is True:
            values['charging_time_estimation'] = self.get_charging_estimation(report)
            
            # Mark wristband as full faster if placed on charger full
            just_placed = self.last_batery_report['in_charger'] is False or placed_on_charger_lately
            marked_as_full = self.last_batery_report.get('charging_time_estimation', 0xff) == 0
            
            is_charging_done_right_now = values['charging_time_estimation'] == 0
            
            if (is_charging_done_right_now or just_placed is True or marked_as_full is True) and report['params']['batteryLevel'] >= 95: # Treat as charging done
                values['charging_time_estimation'] = 0
                values['charging_end_time_estimation'] = self.last_batery_report.get('charging_end_time_estimation', now())
            else:
                values['charging_end_time_estimation'] = now() + timedelta(seconds=values['charging_time_estimation'])
            
            if values['charging_time_estimation'] == 0:
                values['charging_full'] = True
            else:
                values['charging_full'] = (report['params']['charging'] == STATE_FULL)
        
        logger.debug('values:' + str(values))
        return values
    
    def send_alert(self, values, scenario):
        values['alert_scenario'] = scenario
        alert_values = {}
        for k in ['sysid', 'timestamp', 'time', 'source', 'type', 'interactive']:
            alert_values[k] = values[k]
        alert_values['scenario'] = scenario
        mongo_client['events']['mc_in'].insert(alert_values)
        print ('sending alert', scenario)
        print (alert_values)
        logger.debug("Send alert " + scenario + " with values")
        logger.debug(str(values))
        try:
            self.send_mail(scenario, values)
        except:
            import traceback
            logger.debug(traceback.format_exc())
        
    def process_new_battery_report(self, report, placed_on_charger_lately, is_dead, mongo_client):
        msg = 'Processing new report %s: sysid: %s placed_on_charger_lately: %s, is_dead: %s' % (report['timestamp'].ctime(), str(report['sysid']), str(placed_on_charger_lately), str(is_dead))
        print(msg)
        logger.debug(msg)
        logger.debug(str(report))
        
        values = self.get_battery_state(report, is_dead, placed_on_charger_lately)
        values['charging_done'] = self.last_batery_report['charging_done'] # Make charging done sticky  
 
        # Send debug mail for just changed charging_status
        if self.last_batery_report['in_charger'] != values['in_charger']:
            msg = 'WB was put in charger' if values['in_charger'] else 'Took out of charger'
            try:
                self.send_mail(msg, values)
            except:
                import traceback
                logger.debug(traceback.format_exc())
        
        state_changed = True
        
        # Send alerts        
        if values['battery_state'] != self.last_batery_report['battery_state']:
            self.send_alert(values, values['battery_state'])
            
        elif values['battery_state'] not in ['normal_battery', 'battery_charging'] and values['interactive'] is True: # values['battery_state'] == self.last_batery_report['battery_state'] and values['battery_state'] != normal_battery
            # Re-Generate alert
            self.send_alert(values, 'battery_charging')
            values['time'] = values['time'] + 0.001
            self.send_alert(values, values['battery_state'])
      
        elif self.last_batery_report['charging_done']:
            
            if (values['in_charger'] is False or values['charging_full'] is False): # Close charging done alert
                self.send_alert(values, 'charging_complete')
                values['charging_done'] = False 
                
        elif values['in_charger'] is True and values['charging_full'] is True:
            self.send_alert(values, 'charging_done')
            values['charging_done'] = True
        else:
            state_changed = False

        battery_state_changed = compare_records(self.last_batery_report, values, ignore_list = ['_id', 'timestamp', 'time', 'charging_end_time_estimation', 'charging_time_estimation', 'hours_left', 'battery_level', 'battery_level_volts']) is False
        
        if battery_state_changed:
            values.pop('_id', None)
            
            if 'charging_time_estimation' in values and values['charging_time_estimation'] not in [None, 0]:
            
                def update_charging_estimation(cancel_timer = False):
                    global timers
                    if cancel_timer is True and self.sysid in timers:
                        timers[self.sysid].cancel()
                    
                    values = self.get_battery_state(report, is_dead, placed_on_charger_lately)
                    timers[self.sysid] = Timer(min(values['charging_time_estimation']/2, 10*60), update_charging_estimation)    
                    timers[self.sysid].daemon = True
                    timers[self.sysid].start()
                    mongo_client['companion']['battery'].insert(values)
                    
                update_charging_estimation(True)
            else:
                pass
                mongo_client['companion']['battery'].insert(values)
            
        values.pop('_id', None)
        self.last_batery_report = values # set values after db updated successfully
        return state_changed

    def send_mail(self, msg, values):
        #mail = MIMEMultipart('multipart') #multipart|alternative?
        mail = MIMEMultipart('alternative')
        
        #to = ','.join(['odbigler@kyteratech.com', 'assaf.sella@kyteratech.com'])
        to = ','.join(sysids[self.sysid])
        
        message = os.linesep.join(["From: %s" % self.gmail_user,
                        "To: %s" % to,
                        "Subject: %s" % "[Battery-state] System \"%s\" %s" % (self.sysid, msg),
                        "",
                        ] + [('%s: %s' % (str(k), str(v))) for k,v in values.iteritems()])
                        
        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server_ssl.ehlo() # optional, called by login()
        server_ssl.login(self.gmail_user, self.gmail_pwd)
        # ssl server doesn't support or need tls, so don't call server_ssl.starttls() 

        server_ssl.sendmail(self.gmail_user, to, message)
        server_ssl.quit()
        server_ssl.close()

    def get_charging_estimation(self, report):
        charging_estimation_table = {
            0.000000: 135,
            5.000000: 133,
            10.000000: 130,
            15.000000: 127,
            20.000000: 122,
            25.000000: 118.286533,
            30.000000: 115.477867,
            35.000000: 111.650141,
            40.000000: 108.452427,
            45.000000: 103.510196,
            50.000000: 98.564513,
            55.000000: 97.405748,
            60.000000: 93.202926,
            65.000000: 83.269430,
            70.000000: 78.516512,
            75.000000: 69.678049,
            80.000000: 64.238491,
            85.000000: 59.267709,
            90.000000: 51.652834,
            95.000000: 41.596272,
            100.000000: 35.596272,
        }
        
        if report['params']['charging'] not in [STATE_CHARGING, STATE_ZIGZAG, STATE_SUSPECT_ZIGZAG]:
            self.charging_est = {}
            
            if report['params']['charging'] == STATE_FULL:
                return 0
            return None

        # Level changed
        if report['params']['batteryLevel'] != self.charging_est.get('level'):
            #if ('checkpoin_left' not in self.charging_est) and report['params']['batteryLevel'] >= 95: # Battery placed on charger above 95. Send battery done immediatly.
            #    return 0
            self.charging_est['level'] = report['params']['batteryLevel']
            self.charging_est['timestamp'] = now()
            
            levels = charging_estimation_table.keys()
            index = bisect.bisect(levels, self.charging_est['level'])
            if index > 0:
                index = index - 1
            try:
                self.charging_est['checkpoin_left'] = timedelta(minutes = charging_estimation_table[levels[index]])
            except:
                print("exception", index, len(levels), self.charging_est['level'])
                raise

        diff = now() - self.charging_est['timestamp']
        
        if (self.charging_est['checkpoin_left'] - diff) > timedelta(0):
            currently_left = (self.charging_est['checkpoin_left'] - diff).total_seconds()
        else: # Late
            currently_left = 0.9 * self.charging_est.get('last_left_value', (diff - self.charging_est['checkpoin_left']).total_seconds())

        out = (currently_left + self.charging_est.get('last_left_value', currently_left)) / 2
        self.charging_est['last_left_value'] = currently_left
        
        if out < 10.0 * 60:
            out = 5 * 60 + out/2.0

        return out

last_report_time = {}

def check_for_new_battery_reports(mongo_client, last_state_changed):
    event_coll = mongo_client['monitor_events']['system_events']
    global last_report_time, sysids

    def lookback(sysid):
        if sysid not in last_report_time:
            return now() - timedelta(seconds=INTERACTIVE_TIME)
        else:
            return last_report_time[sysid]['timestamp']-timedelta(seconds=30)
        
    def comparator_lower_eq(report1, report2):
        diff = (report1['timestamp']-report2['timestamp']).total_seconds()
            
        if abs(diff) < 0.1 and report1['sysid'] == report2['sysid']:
            return int((report1.get('params', {}).get('uptime', 0xffff) - report2.get('params', {}).get('uptime', 0xffff))*10000)

        return int(diff)
    
    for sysid in sysids:
        #new_reports = event_coll.find({'sysid': sysid, 'timestamp': {'$gt': lookback(sysid), '$lt': now() }, '$or': [{'name': 'EVENT_WB_STATUS'}, {'name': 'EVENT_WRISTBAND_DIE'}]})
        new_reports = event_coll.find({'$or': [{'sysid': sysid, 'timestamp': {'$gt': lookback(sysid), '$lt': now() }, 'name': 'EVENT_WB_STATUS'}, {
                                                'sysid': sysid, 'timestamp': {'$gt': lookback(sysid), '$lt': now() }, 'name': 'EVENT_WRISTBAND_DIE'}]})

        systemBattery = SystemBattery.get(sysid, mongo_client)
        new_reports_cursor = sorted(filter(
            lambda r:r['sysid'] == sysid and (comparator_lower_eq(last_report_time[sysid], r) < 0.0), 
            new_reports), 
            cmp = comparator_lower_eq)
    
        if len(new_reports_cursor) == 0:
            continue

        report = new_reports_cursor[-1]
        last_report_time[sysid] = report
        
        is_dead = False
        
        if report['name'] == 'EVENT_WRISTBAND_DIE': # Check if wristband died
            wri_euiid = [d['euiid'] for d in mongo_client['companion']['systems'].find({'sysid': sysid})[0]['devices'] if d['uid']==10][0]
            if wri_euiid == report['params']['eui'] == wri_euiid:
                is_dead = True
                placed_on_charger_lately = False
            else:
                continue
        if is_dead is False:
            # Respond to change mode events
            placed_on_charger_lately = \
                any(r['sysid'] == sysid and r.get('params', {}).get('charging', -1) in [STATE_ZIGZAG, STATE_SUSPECT_ZIGZAG, PULL_OUT_OF_CHARGER, CHARGER_POWER_OUT]
                    for r in new_reports_cursor)

        if systemBattery.process_new_battery_report(report, placed_on_charger_lately, is_dead, mongo_client):
            last_state_changed = time.time()
            
    return last_state_changed  # Send back last report time.


def check_for_low_battery_on_disconnect_wristbands():
    # This function is intended to deal with the cases the wristband is out of range(system works just fine).
    # In this case, we would like to notify that the wristband might lose its battry so in case of return home, the
    # wristband will be set back to charge.
    # 
    # Note: Here we assume the wristband is disconnected and disconnected to the np.
    # This behavior make sense if the lmuapp reset the np while the server if offline for long period (problem with
    # connection).
    pass

if __name__ == "__main__":
    
    mongo_client = None
    mongo_ip, mongo_port = '10.8.0.1', 27017
    last_state_changed = 0
    
    for i in range(300):
            
        if mongo_client is None:
            try:
                #print 'Connecting to mongo', mongo_ip, mongo_port
                #mongo_client = MongoClient(mongo_ip, mongo_port)
                mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
                
                assert mongo_client is not None
            except:
                import traceback
                traceback.print_exc()
                print('Failed connecting to mongo')
                raise
        try:
            last_state_changed = check_for_new_battery_reports(mongo_client, last_state_changed)
        except pymongo.errors.ConnectionFailure:
            print('Mongo connection error. Reopen mongo connection')
            traceback.print_exc()
            try:
                mongo_client.close()  # Try to close mongo connection first.
            except:
                pass
            mongo_client = None

        if (time.time() - last_state_changed) < 5:
            time.sleep(2)
        else:
            time.sleep(7)


    for t in timers.values():
        t.cancel()
    os._exit(0)


