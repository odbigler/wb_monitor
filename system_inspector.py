import os, socket, glob, time, ctypes
import socket, hashlib, select, traceback
import sys, logging
import logging.handlers
import struct, errno
import threading, time
from pymongo import MongoClient
import pymongo
import datetime, re

import json
from collections import OrderedDict
from datetime import timedelta
from pytz import timezone, utc

LOG_PATH                        = "system_inspector.log"
MONGO_DB_IP                     = '10.8.0.1'
MONGO_DB_PORT                   = 27017
#mongo_client                   = MongoClient(MONGO_DB_IP, MONGO_DB_PORT)
mongo_client                    = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
logger                          = logging.getLogger('System Inspector')

formmater = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

logger.setLevel(logging.DEBUG)
fh = logging.handlers.RotatingFileHandler(LOG_PATH, maxBytes = 10 * 1024 * 1024, backupCount = 5)

fh.setLevel(logging.DEBUG)
fh.setFormatter(formmater)
logger.addHandler(fh)

logger.info("System inspector Started!")

WRISTBAND_CONNECTING_TIME = None

class CheckOutput(object):
    def __init__(self, expected_delay, check_type, alert_msg):
        self.expected_delay = expected_delay
        self.success=False
        self.alert_msg=alert_msg
        self.exc = ''
        self.check_type = check_type
        self.real_delay = 0
        self.result = None
        self.run = False
        self.alert_reason=''
        self.should_alert=None
        self.start_time=''
        self.duration=0
        self._defaults = self.__dict__.keys()
    
    def _serialize_defaults(self, out):
        
        out['check_type'] = self.check_type
         
        if self.run is True:
            out['should_alert'] = self.should_alert
            out['alert_reason'] = self.alert_reason
            out['alert_msg'] = self.alert_msg
            
            if self.success is True:
                out['result'] = self.result
                if self.expected_delay is not None:
                    out['real_delay'] = self.real_delay
                    out['expected_delay'] = self.expected_delay
            else:
                out['success'] = self.success
                out['exc'] = self.exc        
        else:
            out['run'] = self.run
        out['duration'] = self.duration
        
        if self.duration > 1:
            out['start_time'] = self.start_time # string
        return out

    def toSerializableDict(self):
        out = OrderedDict()
        out = self._serialize_defaults(out)
        
        for k, v in self.__dict__.iteritems(): # Add other local dynamic added variables
            if k not in self._defaults and k != '_defaults':
                out[k] = v  
            
        return out
    
    def toJsonSerializableDict(self):
        out = OrderedDict()
        out = self._serialize_defaults(out)
        
        for k, v in self.__dict__.iteritems(): # Add other local dynamic added variables
            if k not in self._defaults and k != '_defaults':
                try:
                    x = json.dumps(v)
                    out[k] = v    
                except:
                    out[k] = traceback.format_exc()
        return out
        
    def repr(self):
        return json.dumps(self.toSerializableDict(), indent=2)
    
    def toMongoDict(self, name, sysid):
        out = OrderedDict()
        out['name'] = name
        out['sysid'] = sysid
        out['timestamp'] = datetime.datetime.utcnow()
        
        out = self._serialize_defaults(out)
        del out['check_type']
        if 'result' in out:
            del out['result']
        params = {}
        for k, v in self.__dict__.iteritems(): # Add other local dynamic added variables
        
            if k in ['_defaults', 'check_type', 'lastIp', 'run', 'exc', 'success', 'expected_delay', 'real_delay']: # Ignore item
                continue

            if k not in ['_defaults'] and k not in out:
                if '_' in k:
                    params[k] = v
                else:
                    out[k] = v
        
        out['params'] = params
        for x in ['alert_reason', 'real_delay', 'expected_delay', 'duration']: # Push to end of order
            if x in out:
                out[x] = out.pop(x)
        
        return out

class CheckSucess(object):
    def __init__(self, name, preTrueConds, preFalseConds, condition_func, expected_delay, explanation='', alert_msg=''):
        self.name = name
        self.alert_msg = alert_msg
        self.condition_func = condition_func
        self.expected_delay = expected_delay
        self.preTrueConds   = preTrueConds
        self.preFalseConds  = preFalseConds
        self.explanation    = explanation
        if alert_msg == '' and explanation != '':
            self.alert_msg = self.explanation

    def shouldAlert(self, out):
        return out.run and out.exc != ''
        
    def alert_reason(self):
        return 'Failed with exception'
        
class CheckResultTrue(CheckSucess):
    """ This class represent all checks that thier result should be true.
    """
    
    def shouldAlert(self, out):
        return out.run and ( CheckSucess.shouldAlert(self, out) or out.result != True)
        
    def alert_reason(self):
        return ''#'Alert result is False or unavailable'
        
class CheckResultFalse(CheckSucess):
    def shouldAlert(self, out):
        return out.run and (CheckSucess.shouldAlert(self, out) or out.result != False)

    def alert_reason(self):
        return ''#'Alert result is True or unavailable'

def utcTime2str(t):
    local = 'Asia/Jerusalem'
    local_t = t.replace(tzinfo=utc).astimezone(timezone(local))
    return str(local_t.isoformat())
    #return "%s %s (%s UTC)" % (localtz.localize(t).ctime(), local, t.ctime())

def run_checks_for_specified_system(syid, check_list, output_file):
    """ Run the conditions one by one and make report output.
    """
    full_report = OrderedDict()
    start_time = datetime.datetime.utcnow()

    for check in check_list:
        out = CheckOutput(check.expected_delay, check.__class__.__name__, check.alert_msg)
        check_start_time = datetime.datetime.utcnow()
        
        preTrueConds =  [(check_name, full_report[check_name]) for check_name in check.preTrueConds]
        preFalseConds = [(check_name, full_report[check_name]) for check_name in check.preFalseConds]
        
        failed_true_prerequisite = filter(lambda (n, c): (not c.run or not c.success or c.result is False), preTrueConds)
        failed_false_prerequisite = filter(lambda (n, c): (not c.run or not c.success or c.result is True), preFalseConds)
        
        if failed_true_prerequisite == [] and failed_false_prerequisite == []:    
            try:
                out.run = True 
                out.result = check.condition_func(sysid, out)
                out.success = True
            except:
                out.exc = traceback.format_exc()
                if 'debug' in sys.argv:
                    raise
        else:
            if failed_true_prerequisite != []:
                out.failed_true_prerequisite = map(lambda (n,c): n, failed_true_prerequisite)
            if failed_false_prerequisite != []:
                out.failed_false_prerequisite = map(lambda (n,c): n, failed_false_prerequisite)

        check_stop_time = datetime.datetime.utcnow()
        out.should_alert = check.shouldAlert(out)
        
        if out.should_alert: #or out.result is False:
            # Add explanation to alert reason.
            if out.alert_reason == '':
                out.alert_reason = check.alert_reason()
        
        out.start_time = utcTime2str(check_start_time)
        out.duration = (check_stop_time - check_start_time).total_seconds()
        full_report[check.name] = out
        
        with file(output_file, 'wb') as f:    
            js = OrderedDict()
            js['start_time'] = utcTime2str(start_time)
            js['start_time_utctime'] = time.mktime(check_start_time.timetuple())
            should_alert = False
            js['tests'] = OrderedDict()

            for res in full_report.keys():
                js['tests'][res] = full_report[res].toJsonSerializableDict()
                
                should_alert = should_alert or js['tests'][res].get('should_alert', False)
                
            js['should_alert'] = should_alert
            js['end_time_utctime'] = time.mktime(check_stop_time.timetuple())
            js['end_time'] = utcTime2str(check_stop_time)
            js['duration'] = (check_stop_time - start_time).total_seconds()

            try:
                json.dump(js, f, indent=2)
                """
                mongo_client['companion']['fmtalert'].find_one_and_update(
                    {'sysid': sysid},
                    {"$inc" : {'alertid' : 1 if should_alert else 0}, 
                     "$set": {'scheme_version': 3, 
                              'sysid' : sysid,
                              'type' : 'malfunction',
                              'status': 'open' if should_alert else 'closed',
                              'timestamp': check_stop_time}
                    }, upsert= True)
                """
                

            except:
                traceback.print_exc()
                #if 'debug' in sys.argv:
                #    IPython.embed()
    try:            
        mongo_client['companion']['malfunction'].insert_many(
                    [full_report[k].toMongoDict(k, sysid) for k in full_report.keys() if full_report[k].run is True])
    except:
        traceback.print_exc()
        
    return full_report

_system = {}

def system(sysid):
    global _system
    if sysid not in _system:
        _system[sysid] = mongo_client['companion']['systems'].find({'sysid': sysid})[0]
    return _system[sysid]
_eui2Name={}
def eui2Name(euiid):
    global _eui2Name
    if euiid not in _eui2Name:
        l=list(mongo_client['companion']['devices'].find({'euiid': euiid}))
        if len(l)>0:
            _eui2Name[euiid] = l[0]['description']
    return _eui2Name.get(euiid, '')

_wristband = {}
def wristband(sys):
    global _wristband
    if sys['sysid'] not in _wristband:
        euiid = [d['euiid'] for d in sys['devices'] if d['uid']==10][0]
        _wristband[sys['sysid']] = mongo_client['companion']['devices'].find({'euiid': euiid})[0]

    return _wristband[sys['sysid']]

def check_lmuapp_checkin(sysid, out):
    sys = system(sysid)
    checkedIn = sys['lastCheckin']
    
    out.real_delay = (datetime.datetime.utcnow() - checkedIn).total_seconds()
    window_title = "%s_%s_%s" % (sys['rpi']['rpiid'], sys['sysid'].replace(' ', '_'), str(sys['lastIp']))
    out.lastIp = """<a href="ssh://kyuser@%s -title %s">%s</a>""" % (str(sys['lastIp']), window_title, str(sys['lastIp']))
    last_restart = sys.get('lastRestart', None)
    just_restarted = False
    if last_restart is not None:
        system_age = (datetime.datetime.utcnow() - last_restart).total_seconds()
        out.recently_restarted = system_age < 30 * 60 # Half a hour
        just_restarted         = system_age < 10 * 60 # Ten minutes
        
        out.last_restart = utcTime2str(last_restart)
    out.channel = sys.get('channelInfo', {}).get('channel', '')
    
    if just_restarted is True:
        out.alert_msg = "LMUApp was re-started at %s" % out.last_restart
        out.user_message = "System is initializing. This can take up to 10 min"
        return False
    out.result = out.real_delay < out.expected_delay
    if out.result is False:
        out.user_message = "System is temporarily not working(the that base station might not be connected to the internet)"
        out.alert_msg = "LMUApp check in failed"
    return out.result
    
def wristaband_is_connected(sysid, out):
    global WRISTBAND_CONNECTING_TIME
    sys   = system(sysid)
    wri   = wristband(sys)
    out.fwVer = "%s.%s" % (wri['fwVer']['major'], wri['fwVer']['minor'])
    
    last_record = list(mongo_client['companion']['wristbands'].find({'activity': {'$ne':'expire'}, 'euiid': wri['euiid'], 'sysid': sysid}).sort('timestamp', -1).limit(1))
    if len(last_record) == 0:
        return False

    last_record = last_record[0]
    
    out.activity = last_record['activity']
    
    out.activity_age = (datetime.datetime.utcnow() - last_record['timestamp']).total_seconds()
    
    if out.activity in ['bootloading','reconnect', 'mode change', 'mode changed', 'reconnected', 'detected'] and out.activity_age >= 20: # Wristband is connected for more than 20 seconds.
        WRISTBAND_CONNECTING_TIME = last_record['timestamp']
        return True
    elif out.activity in ['disconnect', 'timeout']:
        return False
    #else
    if out.activity_age < 20:
        return False
    # Unallowed state for this activity age
    raise Exception("System is in state:%s for %dsec" % (out.activity, out.activity_age))

def check_battery_is_ok(sysid, out):
    batt = list(mongo_client['monitor_events']['system_events'].find({'name': 'EVENT_WB_STATUS', 'sysid': sysid}).sort('timestamp', -1).limit(10))
    die = list(mongo_client['monitor_events']['system_events'].find({'name': 'EVENT_WRISTBAND_DIE', 'sysid': sysid}).sort('timestamp', -1).limit(1))
    
    if len(die) > 0:
        last_die = die[0]['timestamp']
        out.real_delay = (datetime.datetime.utcnow() - batt[0]['timestamp']).total_seconds()
        
        out.last_die = utcTime2str(last_die)
        
        if last_die > batt[0]['timestamp']:
            out.alert_reason = 'Battery died.'
            out.user_message = "System is temporarily not working"
            return False
    
    # Check if battery changed a lot during short time
    
    out.battery_level    = batt[0]['params']['batteryLevel']
    out.wear_detect      = batt[0]['params'].get('wear_detect', '')
    out.alert_indication = batt[0]['params'].get('alert', '')
    
    out.time_period_after_last_report = (batt[0]['timestamp'] - batt[1]['timestamp']).total_seconds()
    out.battery_drain = abs(int(batt[0]['params']['batteryLevel']) - int(out.battery_level))
    
    if out.battery_drain >= 10:
        # The battery lost more then 10 precent lately
        if out.battery_drain < 1.5 * 60 * 60: # Less then 1.5 hour
            # Battery lost 10 precent in less then 1.5 hour
            out.alert_reason = "Battery drained too fast. lost %d%% during %d sec" % (out.battery_drain, out.time_period_after_last_report)
            return False
    return True if any(map(lambda r: r['params']['batteryLevel']>0, batt)) else False

def wristband_is_home(sysid, out):
    last_outdoors = list(mongo_client['algo']['outdoors_events'].find({'sysid': sysid, 'class': {'$in':['return', 'leave']}}).sort('time', pymongo.DESCENDING).limit(1))
    
    if len(last_outdoors) == 0:
        out.output_class = 'return'
        out.alert_reason = 'No records were found'
        return True
        
    out.output_class = last_outdoors[0]['class']
    last_output_timestamp = datetime.datetime.utcfromtimestamp(last_outdoors[0]['time'])
    out.last_output_timestamp = utcTime2str(last_output_timestamp)
    return out.output_class == 'return'
    
def check_out_of_home_algo(sysid, out):
    last_outdoors = list(mongo_client['algo']['outdoors_events'].find({'sysid': sysid}).sort('time', pymongo.DESCENDING).limit(1))
    
    if len(last_outdoors) == 0:
        return True
        
    out.output_class = last_outdoors[0]['class']
    last_output_timestamp = datetime.datetime.utcfromtimestamp(last_outdoors[0]['time'])
    out.last_output_timestamp = utcTime2str(last_output_timestamp)

    if ( datetime.datetime.utcnow() - last_output_timestamp ).total_seconds() < 1 * 60:
        out.last_output_age = datetime.datetime.utcnow() - last_output_timestamp
        return True

    sys   = system(sysid)
    wri   = wristband(sys)

    last_activiy = list(mongo_client['companion']['wristbands'].find({'euiid': wri['euiid'], 'sysid': sysid}).sort('timestamp', -1).limit(1))
    if len(last_activiy) == 0:
        return True
    
    last_activity_timestamp = last_activiy[0]['timestamp']
    out.last_activity_timestamp = utcTime2str(last_activity_timestamp)
    
    if last_activity_timestamp > last_output_timestamp:
        # Ignore the last output
        return True

    out.bad_outputs = ['unknown', 'algo_error']
    if out.output_class not in out.bad_outputs:
        return True

    out.alert_reason = 'Out of home algo finished with status: %s' % out.output_class
    return False

def check_person_is_at_home_is_synced_with_bpacket_data(sysid, out):
    sys   = system(sysid)
    wri   = wristband(sys)

    user_outdoors = list(mongo_client['algo']['user_outdoors'].find({'sysid': sysid}).sort('time', pymongo.DESCENDING).limit(1))
    if len(user_outdoors) == 0:
        out.alert_reason = 'No records at user_outdoors'
        return True
        
    user_outdoors = user_outdoors[0]    
    out.user_outdoors = utcTime2str(datetime.datetime.fromtimestamp(user_outdoors['time']))
    
    out.at_home_indication = user_outdoors['home'] == 'true' or user_outdoors['home'] is True

    out.lastTxDataReceivedByLmu =  wri['lastTxDataReceived']
                                        
    out.data_age_sec = (datetime.datetime.utcnow() - out.lastTxDataReceivedByLmu).total_seconds()
    out.lastTxDataReceivedByLmu = utcTime2str(out.lastTxDataReceivedByLmu)
    
    if out.at_home_indication is True:
        out.data_age_max = 5*60
        out.alert_reason = 'The wristband is missing for an unknown reason'
        # Expected to find packet from the last 12 minutes
        out.result = out.data_age_sec < out.data_age_max
    else:
        out.result = True
        # Person is not at home so expecting to see no data.
        #last_record = list(mongo_client['companion']['wristbands'].find({'euiid': wri['euiid'], 'sysid': sysid}).sort('timestamp', -1).limit(1))
        #if len(last_record) == 0:
        #    return False
        #last_record=last_record[0]
        #out.activity = last_record['activity']
    
        #out.activity_age = (datetime.datetime.utcnow() - last_record['timestamp']).total_seconds()
        
        #if out.activity in ['reconnect', 'mode change']: # wristband is connected
        #    out.wristband_connected_period = out.activity_age
        #    out.result = out.wristband_connected_period < 30 # Pretty new activity
        #    if out.result is False:
        #        out.alert_reason = 'Wristband is already connected for %d sec, although at_home indication indicates NOT at HOME!!' % out.wristband_connected_period
        #else:
        #    out.result = True
    if out.result is False:
        out.alert_msg = "Out-of-home not detected after WB disconnected (probably WB not working)"
        out.user_message = "System is temporarily not working"
    return out.result
    
#def check_wristband_disappeared_recently(sysid, out):
#    sys   = system(sysid)
#    wri   = wristband(sys)
#    
#    out.wristband_last_heard = wri['lastLastHeard']
#    out.wristband_absent_period = (datetime.datetime.utcnow() - out.wristband_last_heard).total_seconds()
#    
#    out.wristband_last_heard = utcTime2str(out.wristband_last_heard)
#    
#    if out.wristband_absent_period_min < 7:
#        return False
#     
#    out.wristband_last_data = mong o_client['companion']['wristbands'].find({"sysid":sysid, "activity": 'timeout'}).sort('timestamp', -1).limit(1)[  0]['timestamp']
#    assert out.wristband_last_data > out.wristband_last_heard + timedelta(min=5), "Coudn't find last_data lately"
#    
#    return out.wristband_absent_period_min > 7 and out.wristband_absent_period <= out.expected_delay
#
#def check_out_of_home(sysid, out):
#    sys   = system(sysid)
#    wri   = wristband(sys)
#    
#    out.last_out_of_home = datetime.datetime.fromtimestamp(mongo_client['algo']['user_outdoors'].find({'sysid': sysid, 'home': 'false'}).sort('time', pymongo.DESCENDING).limit(1)[0]['time'])
#    
#    out.wristband_last_data = mongo_client['companion']['wristbands'].find({"sysid":sysid, "activity": 'timeout'}).sort('timestamp', -1).limit(1)[0]['timestamp']
#    
#    out.real_delay = (out.last_out_of_home - out.wristband_last_data).total_seconds()
#    
#    out.last_out_of_home = utcTime2str(out.last_out_of_home)
#    out.wristband_last_data = utcTime2str(out.wristband_last_data)
#    
#    return out.real_delay > 0 and out.real_delay < out.expected_delay

def wristband_should_sends_accl(sysid, out):
    sys   = system(sysid)
    wri   = wristband(sys)
    out.wri_mode = wri['txMode'].lower()
    return wri['txMode'].lower() in ['distress', 'normal']
    
def wristband_should_sends_tx(sysid, out):
    sys   = system(sysid)
    wri   = wristband(sys)
    out.wri_mode = wri['txMode'].lower()
    return wri['txMode'].lower() in ['distress', 'finger_print']

def wristband_should_have_position_values(sysid, out):
    sys   = system(sysid)
    wri   = wristband(sys)
    out.wri_mode = wri['txMode'].lower()
    return wri['txMode'].lower() in ['distress']
    
def wristband_sends_accl(sysid, out):
    sys   = system(sysid)
    wri   = wristband(sys)
    out.lastAcclDataReceivedByLmu =  wri['lastAcclDataReceived']
    out.real_delay = (datetime.datetime.utcnow() - out.lastAcclDataReceivedByLmu).total_seconds()
    
    if out.real_delay >= out.expected_delay:
        out.alert_reason = 'No accl packet is available since %s' % out.lastAcclDataReceivedByLmu

    out.lastAcclDataReceivedByLmu = utcTime2str(out.lastAcclDataReceivedByLmu)
    return out.real_delay < out.expected_delay

def accl_file_updated(sysid, out):
    wri   = wristband(system(sysid))
    
    if sys.platform == 'win32':
        data_files = r"\\aladdin\data\users"
    else:
        data_files = r"/data/users"
    
    all_accl_files = list(glob.iglob(os.path.join(data_files, sysid, r'data', '*accl.bin')))
    if len(all_accl_files) > 0:
        out.last_updated = datetime.datetime.utcfromtimestamp(
            sorted([os.path.getmtime(f) for f in all_accl_files])[-1])
        out.lastAcclDataReceivedByLmu =  wri['lastAcclDataReceived']
        
        out.WRISTBAND_CONNECTING_TIME=WRISTBAND_CONNECTING_TIME
        
        out.real_delay = (out.lastAcclDataReceivedByLmu - max(out.last_updated, out.WRISTBAND_CONNECTING_TIME)).total_seconds()
        out.WRISTBAND_CONNECTING_TIME = utcTime2str(WRISTBAND_CONNECTING_TIME)
        out.lastAcclDataReceivedByLmu = utcTime2str(out.lastAcclDataReceivedByLmu)
        out.last_updated = utcTime2str(out.last_updated)
        
        if out.real_delay >= out.expected_delay:
            out.alert_reason = 'Too much delay. File last updated at %s' % out.last_updated
            if out.real_delay > 10*60:
                out.user_message = "System is temporarily not working (the base station might not be connected to the internet)"
        return out.real_delay < out.expected_delay
    else:
        out.alert_reason = 'No accl files at all'
        return False

def tx_file_updated(sysid, out):
    wri   = wristband(system(sysid))
    
    if sys.platform == 'win32':
        data_files = r"\\aladdin\data\users"
    else:
        data_files = r"/data/users"
        
    all_tx_files = list(glob.iglob(os.path.join(data_files, sysid, r'data', '*data.bin')))
    if len(all_tx_files) > 0:
        out.last_updated = datetime.datetime.utcfromtimestamp(
            sorted([os.path.getmtime(f) for f in all_tx_files])[-1])
        out.lastTxDataReceivedByLmu = wri['lastTxDataReceived']
        out.WRISTBAND_CONNECTING_TIME = WRISTBAND_CONNECTING_TIME
        out.real_delay = (out.lastTxDataReceivedByLmu - max(out.last_updated, out.WRISTBAND_CONNECTING_TIME)).total_seconds()
        out.lastTxDataReceivedByLmu = utcTime2str(out.lastTxDataReceivedByLmu)
        out.WRISTBAND_CONNECTING_TIME = utcTime2str(out.WRISTBAND_CONNECTING_TIME)
        out.last_updated = utcTime2str(out.last_updated)
        
        if out.real_delay >= out.expected_delay:
            out.alert_reason = 'Too much delay. File last updated at %s' % out.last_updated
            
        return out.real_delay < out.expected_delay
    else:
        out.alert_reason = 'No tx files at all.'
        return False

def wristband_sends_tx(sysid, out):
    sys   = system(sysid)
    wri   = wristband(sys)
    out.lastTxDataReceivedByLmu = wri['lastTxDataReceived']
    
    out.real_delay = (datetime.datetime.utcnow() - out.lastTxDataReceivedByLmu).total_seconds()
    
    if out.real_delay >= out.expected_delay:
        out.user_message = "System is temporarily not working"
        out.alert_reason = 'Wristband is not sending B-packets (while at home) since %s' % out.lastTxDataReceivedByLmu
        
    out.lastTxDataReceivedByLmu = utcTime2str(out.lastTxDataReceivedByLmu)
    return out.real_delay < out.expected_delay

def position_available(sysid, out):
    wri   = wristband(system(sysid))
    
    pos = list(mongo_client['algo']['position'].find({'sysid': sysid}).sort('time', pymongo.DESCENDING).limit(1))
    
    if len(pos) == 0:
        out.alert_reason = 'No position was found at collection position (probably problem with finger print)'
        return False
    
    # Overcame bug that cause the last data to be removed for new data position data to replace it.
    time.sleep(5)
    pos2 = list(mongo_client['algo']['position'].find({'sysid': sysid}).sort('time', pymongo.DESCENDING).limit(1))        
    if pos2[0]['time'] > pos[0]['time']:
        pos = pos2

    out.lastTxDataReceivedByLmu = wri['lastTxDataReceived']

    out.pos1 = utcTime2str(datetime.datetime.utcfromtimestamp(pos[0]['time']))
    out.pos2 = utcTime2str(datetime.datetime.utcfromtimestamp(pos2[0]['time']))
    out.WRISTBAND_CONNECTING_TIME=WRISTBAND_CONNECTING_TIME
    
    out.last_position_time = datetime.datetime.utcfromtimestamp(pos[0]['time'])
    
    if sys.platform == 'win32':
        room_list  = r'C:\git\config\room_list.json'
    else:        
        room_list  = r'/home/ubuntu/position_fp/config/global/room_list.json'

    num2roomName = {}
    with file(room_list,'rb') as f:
        for k, v in json.loads(f.read()).iteritems():
            num2roomName[v] = k

    out.last_position_room = str(num2roomName.get(pos[0]['rooms'][0], 'Invalid room name:%s' % str(pos[0]['rooms'])))
    out.real_delay = (out.lastTxDataReceivedByLmu - max(out.WRISTBAND_CONNECTING_TIME,out.last_position_time)).total_seconds()
    
    out.last_position_time = utcTime2str(out.last_position_time)
    out.WRISTBAND_CONNECTING_TIME = utcTime2str(out.WRISTBAND_CONNECTING_TIME)
    out.lastTxDataReceivedByLmu = utcTime2str(out.lastTxDataReceivedByLmu)
    
    if out.real_delay < out.expected_delay:
        return True

    if out.real_delay > 5 * 60 * 60:
        out.alert_reason = 'No position is available since %s' % out.last_position_time    
    else:
        out.alert_reason = 'Position delay is %d minutes' % (out.real_delay/60.0)

    out.user_message = "System is temporarily not working"
    
    return False

def posture_available(sysid, out):
    sys   = system(sysid)
    wri   = wristband(sys)
    out.lastAcclDataReceivedByLmu =  wri['lastAcclDataReceived']
    pos = list(mongo_client['algo']['posture'].find({'sysid': sysid}).sort('time', pymongo.DESCENDING).limit(1))
    if len(pos) == 0:
        out.lastAcclDataReceivedByLmu = utcTime2str(out.lastAcclDataReceivedByLmu)
        out.alert_reason = 'No posture was found at collection posture for sysid:%s' % sysid
        return False
    time.sleep(5)
    pos2 = list(mongo_client['algo']['posture'].find({'sysid': sysid}).sort('time', pymongo.DESCENDING).limit(1))
    if pos2[0]['time'] > pos[0]['time']:
        pos = pos2
        
    out.last_posture = max(WRISTBAND_CONNECTING_TIME, datetime.datetime.fromtimestamp(pos[0]['time']))
    out.WRISTBAND_CONNECTING_TIME = utcTime2str(WRISTBAND_CONNECTING_TIME)
    out.real_delay = (out.lastAcclDataReceivedByLmu - out.last_posture).total_seconds()
    out.lastAcclDataReceivedByLmu = utcTime2str(out.lastAcclDataReceivedByLmu)
    out.last_posture = utcTime2str(out.last_posture)
    stance2Name = {0: 'idle', 1: 'lie', 2: 'sit', 3: 'stand', 4: 'walk', 5: 'rush'} 
    out.last_stance = str(stance2Name.get(round(float(pos[0]['stance'])), 'Invalid stance'))
    
    if out.real_delay < out.expected_delay:
        return True
        
    out.user_message = "System is temporarily not working"
    
    if out.real_delay > 5 * 60 * 60:
        out.alert_reason = 'No posture is available since %s' % out.last_posture
    else:
        out.alert_reason = 'Posture delay is %d minutes' % (out.real_delay/60.0)
    return False
    
def check_all_sensors_connected(sysid, out):
    activateSensors = [d for d in list(mongo_client['companion']['devices'].find({'type':'sensor', 'isActivated':True}))]
    activateSensorsEuiids = [d['euiid'] for d in activateSensors]
    system_sensors_euiids = [d['euiid'] for d in (system(sysid)['devices']) if d['uid'] not in  [0, 10]] # All devices exculed uid 0 and 10
    
    missing_sensors_euiids = filter(lambda eui: eui not in activateSensorsEuiids, system_sensors_euiids)
    
    out.missing_sensors = map(eui2Name, missing_sensors_euiids)
    out.alert_msg = 'Some sensors are not connected'
    out.user_message = "System is temporarily not working (please make sure all sensors are connected to power outlet)"
    return out.missing_sensors == []
    
def wear_detect_works(sysid, out):
    last_wear_detect_result = list(mongo_client['monitor_events']['debug'].find({'sysid':sysid, 'params.debugString':  {'$regex' : 'A1='} }).sort('timestamp', pymongo.DESCENDING).limit(1))
    
    if len(last_wear_detect_result) == 0:
       return True
       
    out.last_wear_detect_result = last_wear_detect_result[0]['params']['debugString']
    out.A1 , out.A2 = map(int, re.match('.*A1=(\d+).*A2=(\d+).*', out.last_wear_detect_result).groups())
    
    if out.A1-out.A2 < 20:
        out.alert_reason = 'Problem with the wear detect values.'
        return False
    return True

def get_check_list():
    return [
        # Basic
        CheckResultTrue('LmuApp checkin', [], [], check_lmuapp_checkin, 5 * 60, 'Check remote lmuApp check-in timestamp is up to date.', alert_msg = 'Connection problem. might be internet issue.'),
        CheckSucess('battery is ok', ['LmuApp checkin'], [], check_battery_is_ok, None, 'Check if the battery level is above 10', alert_msg = 'No battery . Please charge wristband/'), 
        CheckSucess('wristband is connected', ['LmuApp checkin'], [], wristaband_is_connected, None, 'Check the wristbad is connected for more than 20 seconds. Use wristbands collection last activity and it\'s time.'),
        
        #CheckResultTrue('wear detecte works', ['LmuApp checkin'], [], wear_detect_works, None, 'Check if the wear detect machenism works'),
        
        # Position
        CheckResultTrue('All sensors connected', ['LmuApp checkin'], [], check_all_sensors_connected, None, 'Check all defined sesnors are marked as activated.', alert_msg = 'Problem with some sensors. Please check all sensors are properly powered.'),
        
        CheckSucess('Wristband is at home', ['wristband is connected'], [], wristband_is_home, None, 'Use out of home algo result to assure the wristband is back home.'),
        
        CheckSucess('Wristband current mode includes rssi data', ['Wristband is at home'], [], wristband_should_sends_tx, None, 'Check if wristband mode is finger-print or distress'),
        CheckSucess('Wristband current mode includes position', ['Wristband is at home'], [], wristband_should_have_position_values, None, 'Check if wristband mode is normal or distress'),
        CheckResultTrue('Wristband sends rssi data', ['wristband is connected', 'Wristband current mode includes rssi data'], [], wristband_sends_tx, 5*60, 'Lmu-App last tx data is up to date.'),
        CheckSucess('Rssi file is updated', ['wristband is connected', 'Wristband sends rssi data'], [], tx_file_updated, 5*60, 'Wristband data files was modified lately.'),
        CheckResultTrue('New position is available', ['wristband is connected', 'All sensors connected', 'Rssi file is updated', 'Wristband current mode includes position'], [], position_available, 5*60, 'Last position record(in db) timestamp is up to date.', alert_msg = 'Problem with location tracking. We will contact you soon.'),

        # Posture
        CheckSucess('Wristband current mode include accl data', ['Wristband is at home'], [], wristband_should_sends_accl, None, 'Check if wristband mode is normal or distress'),
        CheckResultTrue('Wristband sends accl data', ['wristband is connected', 'Wristband current mode include accl data'], [], wristband_sends_accl, 5*60, 'Lmu-App last accl data is up to date.'),
        CheckSucess('Accl file is updated', ['wristband is connected', 'Wristband sends accl data'], [], accl_file_updated, 5*60, 'Wristband accl files was modified lately.'),
        CheckResultTrue('Rsync is working', ['wristband is connected', 'Wristband sends accl data'], ['Accl file is updated'], accl_file_updated, 17*60, 'RSYNC manage to update file'),
        CheckResultTrue('New posture is available', ['wristband is connected', 'Accl file is updated'], [], posture_available, 5*60, 'Last posture record(in db) timestamp is up to date.'), 
        
        # Out of Home
        CheckResultTrue("Out of home output", ['LmuApp checkin'], ['wristband is connected'], check_out_of_home_algo, None, "Checks the output of the out of home algo", alert_msg = "Problem out of home algo"),
        
        CheckResultTrue("Out of home indication", ['LmuApp checkin', 'battery is ok'], [], check_person_is_at_home_is_synced_with_bpacket_data, None, "Check the last out of home indication and match it with last acc data age(how old the last sample is)"),
        #CheckSucess('Wristband disappeared recently', ['LmuApp checkin', 'battery is ok'], ['wristband is connected'], check_wristband_disappeared_recently, None, 'Check if wristband disappeared recently(7-10 minutes ago)'),
        #CheckResultTrue('Wristband is out of home', ['Wristband disappeared recently', 'battery is ok'], [], check_out_of_home, 2*60, "Check if there was last data")
    ]

def run_tests(sysid, output_file):
    check_list = get_check_list()
    run_checks_for_specified_system(sysid, check_list, output_file)

if __name__ == "__main__":
    sysid, output_file = sys.argv[1], sys.argv[2]
    run_tests(sysid, output_file)

