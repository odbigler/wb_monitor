import time, os, smtplib
import traceback
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders

import pymongo
from pymongo import MongoClient
from utils import config_service
import sys, logging
import logging.handlers
from threading import Timer
import bisect

def main(mongo_client, sysid):
    devices = mongo_client['companion']['systems'].find_one({'sysid':sysid})['devices']
    
    wristband = [d['euiid'] for d in devices if d['uid'] == 10][0]
    #np = [d['euiid'] for d in devices if d['uid'] == 0][0]
    sensors = [d['euiid'] for d in devices if d['uid'] not in [0, wristband]]
    
    t = datetime.utcnow() #- timedelta(days=2, hours = 11)
    
    sensor_status = list(mongo_client['monitor_events']['system_events'].find(
        {"name" : "EVENT_SENSOR_STATUS", 'sysid': sysid,
         "timestamp": {"$lt": t, "$gt": t - timedelta(minutes=10) } }, 
       {"params.euiid":1, "params.parent_euiid":1} ))
    
    parents = {}
    for sensor in sensors:            
        parents[sensor] = list(set([f['params']['parent_euiid'] for f in sensor_status if f['params']['euiid'] == sensor]))
        
    np_euiid = set().union(*parents.values())-set(sensors); assert len(np_euiid)==1; np_euiid=list(np_euiid)[0]
    childs = {}
    missing_euiids = []
    for sensor, parent in parents.iteritems():
        if len(parent) == 0: #sensors is missing
            missing_euiids.append(sensor)
        else:
            assert len(parent) == 1, "Sensors %s was connect to more then one parent" % sensor + str(parent)
            childs.setdefault(parent[0], []).append(sensor)
    
    print 'Nodes topology graph'
    
    def print_node(curr_node, tab):
        print tab*"\t", curr_node
        for child in childs.get(curr_node, []):
                print_node(child, tab+1)
    print_node(np_euiid, 0)

if __name__ == "__main__":
    sysid = sys.argv[1]
    mongo_client = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")
    main(mongo_client, sysid)
