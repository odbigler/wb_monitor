import datetime
import os, socket, glob, time, ctypes
import socket, hashlib, select, traceback
import sys, logging
import logging.handlers
import struct, errno
import threading, time
from pymongo import MongoClient
import pymongo
import datetime, re
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders
import json
from collections import OrderedDict
from datetime import timedelta
from pytz import timezone, utc
import json
import urllib2
import smtplib

HOSTNAME = socket.gethostname()
PORT_NUMBER = int(sys.argv[1])
INSTANCE_NAME = "Troubleshooter" + '_' + socket.gethostname()+ '_' + str(PORT_NUMBER)

mongo_client                    = MongoClient("""mongodb://ubuntu:ubuntu123456@ds143955-a0.mlab.com:43955,ds143955-a1.mlab.com:43955/admin?replicaSet=rs-ds143955""")


class TroubleShooterOutOfSensorPowerSource(object):
    def __init__(self, sysid):
        self._save_to_db = False
        self.sysid = sysid
        self.elder = mongo_client['companion']['systems'].find_one({'sysid':self.sysid})['users'].get('elder', None)
        self.elder_phone = None
        
        self.to = ['odbigler@kyteratech.com']
        self.gmail_user = 'KyteraMailer@gmail.com'
        self.gmail_pwd = 'nbGu8Db3k1'

        if self.elder is not None:
            self.elder_phone = mongo_client['companion']['users'].find_one({'username': self.elder})['phoneCell']
        
        self.ctx = None
        
        self._load_context()
        
        # If not context in db
        if self.ctx is None:
            self._initialize()
            self._do_save_changes()
            
    def _initialize(self):
        sensors = {}
        systemObj = mongo_client['companion']['systems'].find({'sysid': self.sysid})[0]
        system_sensors_euiids = [d['euiid'] for d in (systemObj['devices']) if d['uid'] not in  [0, 10]] # All devices exculed uid 0 and 10
        
        for euiid in system_sensors_euiids:
            sensors[euiid] = {'power': 'unknown'}
            
        self.ctx = {
            'sensors': sensors,
            'sysid': self.sysid,
            'server_instance': INSTANCE_NAME,
            'ctx_id': 0,
        }
        
        self._save_changes(False) # Create new context
        
    def _generate_new_alert(self, target_sensor):
        # Generates the Dashboard-Alert associated with the power - Issue of the sensor
        pass
    
    def _get_last_power_status(self, tail=0):
        return list(mongo_client['companion']['malfunction'].find({'name': 'Sensors are properly powered', 'sysid': self.sysid }).sort('timestamp', -1).limit(tail+1))[-1]
    
    def _load_context(self):
        ctx = list(mongo_client['companion']['troubleshooter'].find({'sysid': self.sysid, 'server_instance': INSTANCE_NAME}).sort('timestamp', -1).limit(1))
        if ctx != []:
            self.ctx = ctx[0]
            del self.ctx['_id']
        
    def _save_changes(self, update_context = True):
        self._update_context = update_context
        self._save_to_db = True
        
    def _do_save_changes(self):
        if self._save_to_db is True:
        
            if self._update_context is False:
                self.ctx['ctx_id'] = self.ctx['ctx_id'] + 1
                self.ctx['timestamp']=datetime.datetime.utcnow()
                mongo_client['companion']['troubleshooter'].insert(self.ctx)
                
            else:
                self.ctx['timestamp']=datetime.datetime.utcnow()
                
                mongo_client['companion']['troubleshooter'].find_one_and_update(
                {'sysid': self.sysid, 'server_instance': INSTANCE_NAME, 'ctx_id': self.ctx['ctx_id'] },
                {"$set": 
                    self.ctx
                 })
                
    def periodicTickAndSaveContext(self):
        self._save_to_db = False
        self.periodicTick()    
        self._do_save_changes()
    
    def sms_ask_elder_to_check_sensor(self, sensor_name, ctx, sensors_to_report_to_elder):
        
        if len(sensors_to_report_to_elder) == 1:
            message = 'Sensor %s is not powered by the mains and uses battery. Please make sure the power supply is properly connected.'
        elif len(sensors_to_report_to_elder) > 1:
            message += 'Sensors at %s are not powered by the mains and use battery. Please make sure the power supply is properly connected.'
        
        if ctx['active_unplug'] is True:
            prefix = 'Sensor %s was unplugged from the mains %d minutes ago. Please re-plug it.' % (sensor_name, (datetime.datetime.utcnow() - ctx['moved_to_battery_time']).total_seconds()/60.0 )
            if len(sensors_to_report_to_elder) == 1:
                message = prefix
            else:
                message = "%s.\n%s" % (prefix, message)
                
                
        #if self.elder_phone is not None:
        #    data = {"params": [{"username": "oded", "to" :  self.elder_phone, "message": message}]}
        #    
        #    import requests
        #    r = requests.post(r"http://10.8.0.1:33331/sms", data=data)
        #    print(r.status_code, r.reason)
        #
        #    req = urllib2.Request(r"http://10.8.0.1:33331/sms")
        #    req.add_header('Content-Type', 'application/json')
        #    response = urllib2.urlopen(req, json.dumps(data))

        msg = MIMEMultipart('multipart') #multipart|alternative?
        msg['From'] = self.gmail_user
        msg['To'] = ','.join(self.to)

        msg['Subject'] = "[Sensor-power issue] System \"%s\"" % (self.sysid)
        
        msg.attach( MIMEText(message, 'plain'))
        
        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server_ssl.ehlo() # optional, called by login()
        server_ssl.login(self.gmail_user, self.gmail_pwd)
        # ssl server doesn't support or need tls, so don't call server_ssl.starttls() 
        print 'Sending Mail to: %s' % msg['To']

        server_ssl.sendmail(self.gmail_user, self.to, msg.as_string())
        server_ssl.quit()
        server_ssl.close()
        
        ctx['reported_to_elder_by_sms'] = {'response': 'send to mail'}

    def periodicTick(self):
        # Got to stage and feed it with new open_alert, new close_alerts, new_unkown_alerts
        last_record = self._get_last_power_status()
        
        for sensor_name, mode in last_record['params']['sensors_power'].iteritems():
            print sensor_name, mode
            sensor_ctx = self.ctx['sensors'][sensor_name]
            
            if sensor_ctx['power'] in ['mains', 'unknown'] and mode == 'battery':
                active_unplug = False
                
                one_before = self._get_last_power_status(1)
                
                if last_record['timestamp'] - one_before['timestamp']  < datetime.timedelta(minutes=5) and \
                    one_before['params']['sensors_power'][sensor_name] == 'mains':
                    active_unplug = True
                
                new_sensor_ctx = { 'power': 'battery',
                              'sensor_euiid': sensor_name,
                              'active_unplug': active_unplug,
                              'reported_to_elder_by_sms': False,
                              'moved_to_unkown_timestamp': None,
                              'reported_to_caregiver_by_sms': False,
                              'moved_to_battery_time': last_record['timestamp']}
                              
                self.ctx['sensors'][sensor_name] = new_sensor_ctx # Create new record on db.
                
                print 'Found new sensor disconnected from mains' , sensor_name
                
                self._save_changes(False) # Create new context
                
            elif sensor_ctx['power'] in ['battery', 'unknown'] and mode == 'mains':
                # The sensor is back connected to the mains
                sensor_ctx['power'] = 'mains'
                sensor_ctx['sensor_back_to_mains_time'] = last_record['timestamp']
                
                print 'Sensor is back powered by mains' , sensor_name
                self._save_changes()
                
            elif sensor_ctx['power'] == 'mains' and mode == 'unknown':
                # Is it the first time
                if sensor_ctx['moved_to_unkown_timestamp'] is None:
                    sensor_ctx['moved_to_unkown_timestamp'] = last_record['timestamp']
                    sensor_ctx['power'] = 'unknown'
                    print 'Sensor moved to unknown powered state' , sensor_name
                    self._save_changes()

        sensors_to_report_to_elder = filter(lambda (name, ctx): ctx['power'] == 'battery' and ctx['reported_to_elder_by_sms'] is False, self.ctx['sensors'].iteritems() )
        
        if len(sensors_to_report_to_elder) > 0:
            # Take oldest
            sensor_name, ctx = sorted(sensors_to_report_to_elder, key = lambda (name, ctx): ctx['moved_to_battery_time'])[0]
            
            if datetime.datetime.utcnow() - ctx['moved_to_battery_time'] > datetime.timedelta(minutes=1):
                # Send sms message
                self.sms_ask_elder_to_check_sensor(sensor_name, ctx, sensors_to_report_to_elder)
                self._save_changes()
                
        # First we fix the state for all sensors
        # Open dashboard-alert for new ones
        # Close dashboard-alert for close ones
        # Ignore the unkowns status ??
        # Maybe after the device is in unkown state for some time, nofity the fmt-alert about the state. 
        
        # Seconds
        # We have some context for each sensor.
        # Look for all the sensor that aren't reported by that are using battery and detection time for it is the oldest
        # If this time is more then five minutes ago send sms message about this sensor, and all other sensor that are in battery mode
        # Mark all sensors as reported by the sms message
        
        
 #   def stage_0(self, new_open_alert, new_close_alerts, new_unkown_alerts):
 #       # Detect new issue with sensor.
 #       
 #       obj = self._get_last_power_status()
 #       
 #       sensors_on_battery = set(filter(lambda (_,x): x == 'battery',obj['params']['sensors_power'].iteritems() ))
 #       
 #       if len(sensors_on_battery) < 0:
 #           return
 #           
 #       target_sensor = sensors_on_battery[0]
 #       
 #       # Open new alert
 #       # Check if the sensor was unplugged.
 #       activly_unplugged_flag = self._check_sensor_was_activly_unplugged(target_sensor, obj['timestamp'])
 #       
 #       self._open_new_sensor_is_on_battery_alert(sensors_on_battery[0], activly_unplugged_flag)
 #       
 #       # Ctx is saved to DB
 #       self.ctx.clear() # Ignore the old context, generate new one.
 #       self.ctx.target_sensor = target_sensor
 #       self.ctx.activly_unplugged_flag = activly_unplugged_flag
 #       
 #       self.move_stage(1)
 #       
 #   def stage_1(self):
 #       # Wait for sms sending hysteresis
 #       obj = self._get_last_power_status()
 #       sensors_on_battery = set(filter(lambda (_,x): x == 'battery',obj['params']['sensors_power'].iteritems() ))
 #       
 #       if len(sensors_on_battery) == 0:
 #           # Close immediately
 #           
 #       if datetime.datetime.utcnow() - self.ctx.get_stage_started_time() < 5*60:
 #           return
 #       
 #       # Send the sms message
 #       obj = _get_current_power_status(self)
        

#class TroubleShootSensorGotOutPower(object):
#    # Handle the case where only one sensor was plugged out from mains to battery.
#    # Make sure the connection is not zigzaging between more scenarios.
#    # Make sure the case is unique. If its more then one sensor close this troubleshooting. 
#    
#    def __init__(self, sysid, euiid_sensor, move_to_battery_time):
#        self.sysid = sysid
#        self.euiid_sensor = euiid_sensor
#        self.move_to_battery_time = move_to_battery_time
#
#    def fetch_last_power_status(self):
#       return list(mongo_client['companion']['malfunction'].find({'name': 'Sensors are properly powered', 'sysid': self.sysid}).sort('timestamp', -1).limit(0))[0]['params']['sensors_power'] 
#
#    def check_new_status(self, status):
#        status =  fetch_last_power_status()
#        sensors_on_battery = set(filter(lambda x: x == 'battery',status))
#        
#        if len(sensors_on_battery) == 0:
#            return self.close()
#        
#        if set(filter(lambda x: x == 'battery',status)) != set([self.euiid_sensor]):
#            # More then one sensor, send it to another troubleshooter
#            raise Exception('Not implemented...')
#        
#        # If enough time passed we can send an sms.
#    
#    def stage_one(self): # The sensors is disconnected but the subscriber might fix it by its own.
#        # If the battery is really low 
#        
#        if self.is_closed():        
#            if self.count('stage_one_close_delay') < datetime.timedelta(minutes = 1):
#                yield
#
#            self.notify_sensor_is_connected_to_mains()
#            self.close_immediately()
#            
#        if self.count('before_sms') < datetime.timedelta(minutes = 10):
#            return
#        # Send sms
#        self.move_stage(2)
#    
#    def stage_two(self):
#        if self.is_closed():
#        
#            if self.count('stage_two_notify_delay') < datetime.timedelta(minutes = 1):
#                yield
#                
#            self.notify_sensor_is_connected_to_mains()
#            self.move_stage(3)
#            
#            if self.count('before_close') < datetime.timedelta(minutes = 30):
#                self.close_immediately()
#        else:
#            self.reset_count('before_close')
#
#    def stage_three(self):
#        # Hystersis waiting to make sure the problem doesn't happen again
#        
#        if not self.is_closed():
#            # The sensor is not connected again...
#            self.notify_sensor_is_not_connected_to_mains() # This time to notification message is "The sensor got disconnected %d times from time %s".
#            
#        if self.count('before_close') < self.time_from_last_event(): # Double the time before the previous event closing.
#            yield
#        
#        self.close_immediately() # Close the event
#
#    # Another strategy:
#    # On a short power on battery igonre it.
#    # If the time on battery was long, maake sure the battery 
#    # We can make a tracker on the sensor battery issue.
#    # The tracker maintain a logic of how to send the sms messages.
#    # The tracker is different from the "alerts" objects. It has it's own object. Which points to the alert it created.
#    # The tracker looks on a long term, and generate this alert lines respectivaly. 
#    # The formmater will use the alerts object to generate the log lines on dashboard.
#    # The formmater will take the alert on directly starts 
#    
#    
#    
#    
#    
#    
#    



if __name__ == "__main__":
    x=TroubleShooterOutOfSensorPowerSource('oded_alpha5')
    x.periodicTickAndSaveContext()
    time.sleep(5)
    
    