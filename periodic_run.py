import sys, os, time, random, smtplib, traceback
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email import Encoders

period = float(sys.argv[1])
command = ' '.join(sys.argv[2:])

file_name = 'period_run%d.out' % random.randint(0, 100000000)

def send_mail(ret):
    gmail_user = 'KyteraMailer@gmail.com'
    gmail_pwd = 'nbGu8Db3k1'
    
    try:
        mail = MIMEMultipart('alternative')
        to = ','.join([gmail_user, ])
        message = os.linesep.join(["From: %s" % gmail_user,
                                "To: %s" % to,
                                "Subject: Command Crashed %d %s" % (ret, command),
                                ""]+file(file_name,'rb').readlines())

        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server_ssl.ehlo() # optional, called by login()
        server_ssl.login(gmail_user, gmail_pwd)
        # ssl server doesn't support or need tls, so don't call server_ssl.starttls() 

        server_ssl.sendmail(gmail_user, to, message)
        server_ssl.quit()
        server_ssl.close()
    except:
        traceback.print_exc()

while True:
    import subprocess

    try:
        proc = subprocess.Popen(command + ' > %s 2>&1' % file_name, shell=True,  stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        while proc.poll() is None:
            print proc.stdout.readline()
        if proc.poll() != 0:
            print 'sending mail. Exit code ', proc.poll()
            send_mail(proc.poll())
    except KeyboardInterrupt:
        print "Got Keyboard interrupt"
        print file(file_name, 'rb').read()
        #time.sleep(1)
        os.unlink(file_name)
        os._exit(0)

    time.sleep(period)


